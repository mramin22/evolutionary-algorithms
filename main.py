from numpy import random
import math
from random import gauss





class config:
    n = 2
    generationCount = 20
    populationCount = 30
    # create children 7 times than parents
    childrenValue = 7
    cValue = -1
    # this value is due to choose which function to use
    # 1 is function on ,2 is function two, and 3 is the third function
    functionSelection = 3


class Chromosom:

    def __init__(self,newGene = []):
        # if we didn't pass the gene, make new one randomly
        if(newGene == []):
            self.geneArray=[]
            self.fitness = -1
            
            for x in range(config.n):
                self.geneArray.append(random.uniform(-10,10))
        else:
            self.geneArray = newGene
            self.fitness = -1





class ChooseClass():
    # selectedPopulationCount is the population that we're gona select
    def __init__(self, selectedPopulationCount: int):
        self.selectedPopulationCount = selectedPopulationCount

    
    def sus(self,parentAndChildrenArray):
        # probabilityArray is our lineArray that it's value is from 0 to 1 
        copiedArray = parentAndChildrenArray[:]
        probabilityArray = self.__calculateProbability(copiedArray)
        pointer = random.uniform(0, 1 /self.selectedPopulationCount)

        selectedPopulation = []
        index = 0
        value = 0
        # now we will choose the items below
        # while(pointer <= 1):
            
        while index < len(probabilityArray):
            chosenIndex = -1
            if pointer > probabilityArray[index]:
                # if the value was more than pointer select this from population
                chosenIndex = index
                index +=1
                # break
            else :
                # index +=1
                pointer += 1/ self.selectedPopulationCount
            
            # iterate pointer with the step of 1/populationCount
            if chosenIndex != -1:
                selectedPopulation.append(parentAndChildrenArray[chosenIndex])
                
        
        return selectedPopulation
        
        

    def __calculateProbability(self,items):
        # we must have the probability array to use the choose algorithms
        # return type would be an array of items and their probability

        # calculate the sum of fitness's
        sumValue = 0
        for i in range(0,len(items)):
            sumValue += items[i].fitness

        # create array of probabilities
        # one of the index of probabilityArray is like -> (0.15,0.45,0.80,1) 
        probabilityArray = []
        totalValues = 0
        for index in range(0,len(items)):
            totalValues += (items[index].fitness / sumValue)
            probabilityArray.append( totalValues )
        
        return probabilityArray
        

            

    def roulette_wheel(self,parentAndChildrenArray,number : 5):
        line=[(parentAndChildrenArray[0],parentAndChildrenArray[0].fitness)]

        for i in parentAndChildrenArray[1:]:
            line.append((i,line[-1][1]+i.fitness))
        selected_list = []
        for x in range(number):
            point=line[-1][1]*rnd()
            for t in line:
                if point < t[1]:
                    selected_list.append(t[0])
                    break
            return selected_list
    
    def tournomant(self, parentAndChildrenArray,withReplacement: bool):
        # first of all we would calculate the tournament size (k value)
        # by deviding the parentAndChildrenArray size on selectedPopulationCount we can get the tournament net size (k value)
        k = len(parentAndChildrenArray) / self.selectedPopulationCount

        selectedPopulation = []

        # get a copy of parentAndChildrenArray
        parentAndChildrenArrayCopy = parentAndChildrenArray[:]
        for idx in range(0, self.selectedPopulationCount ):
            # we won't create an array instead we would create a simple variable name array
            # with that variable we would select the best of population in a tournament net
            array = None
            for index in range(0,int(k)):

                rand = random.randint(0, len(parentAndChildrenArrayCopy)-1 )
                if array!= None:
                    # we should select the best fitness in tournoment 
                    fitnessValue = array.fitness
                    if parentAndChildrenArrayCopy[rand].fitness > fitnessValue:
                        array = parentAndChildrenArrayCopy[rand]
                else:
                    # if it was the first time, select the selected item 
                    array = parentAndChildrenArrayCopy[rand]
                # if the algorithm was without replacement we would pop the item from parentAndChildrenArrayCopy
                if withReplacement == False:
                    #pop it from parentAndChildrenArrayCopy 
                    parentAndChildrenArrayCopy.pop(rand)
            
            # add the chosen item to selectedPopulation array
            selectedPopulation.append(array)
        return selectedPopulation


    def calculateFitnessFunctionOne(self, population):
        # first of all we would get the population array as input
        # then we would start the process
        valueOne = 10 * config.n
        valueTwo = 0
        # 
        if isinstance(population, list):
            for i in range(0,len(population)):
                for j in range(0,config.n):
                    valueTwo += (population[i].geneArray[j] * population[i].geneArray[j]) - 10*math.cos( math.pi * population[i].geneArray[j])
                # print('index ',i ,' value\t',valueOne+valueTwo)
                population[i].fitness = 1 / (1 + valueTwo+valueOne)
        else:
            for j in range(0,config.n):
                valueTwo += (population.geneArray[j] * population.geneArray[j]) - 10*math.cos( math.pi * population.geneArray[j])
            # print('Fitness value\t',valueOne+valueTwo)

            population.fitness = 1 / (1 + valueTwo+valueOne)
        
        return population
    
    def calculateFitnessFunctionTwo(self, population):
        a = 2 # a
        b = 4 # b
        d = config.n # d
        c = 2 # c
        sigma1 = 0
        sigma2 = 0
        # if input was an array of populations or else ,is just a person in population
        if isinstance(population, list):
            for person in range(0,len(population)):
                for i in range(0,d): # for calculating sigma
                    sigma1 += (population[person].geneArray[i]*population[person].geneArray[i]) # calculate first Sigma
                    sigma2 += math.cos(c*population[person].geneArray[i]) # calculate second sigma
                formula = -1 * a * math.exp(-1 * b * math.sqrt((1/d)*sigma1)) - math.exp((1/d) * sigma2) + a + math.exp(1) # second formula
                population[person].fitness = 1/formula
        else:
            for i in range(0,d): # for calculating sigma
                sigma1 += (population.geneArray[i]*population.geneArray[i]) # calculate first Sigma
                sigma2 += math.cos(c*population.geneArray[i]) # calculate second sigma
            formula = -1 * a * math.exp(-1 * b * math.sqrt((1/d)*sigma1)) - math.exp((1/d) * sigma2) + a + math.exp(1) # second formula
            population.fitness = 1/formula
        return population

    def calculateFitnessFunctionThree(self, population):
        sigma = 0

        if isinstance(population, list):
            for person in range(0,len(population)):
                for i in range(0,config.n):
                    if population[person].geneArray[i] > 5.12 or population[person].geneArray[i] < -5.12:
                        sigma += 10 * population[person].geneArray[i] * population[person].geneArray[i]
                    elif population[person].geneArray[i] < 5.12 and population[person].geneArray[i] > -5.12:
                        sigma += (population[person].geneArray[i] * population[person].geneArray[i]) - 10 * math.cos(2*math.pi*population[person].geneArray[i])
                formula = 10 * config.n + sigma
                population[person].fitness = 1/formula
        else:
            for i in range(0,config.n):
                if population.geneArray[i] > 5.12 or population.geneArray[i] < -5.12:
                    sigma += 10 * population.geneArray[i] * population.geneArray[i]
                elif population.geneArray[i] < 5.12 and population.geneArray[i] > -5.12:
                    sigma += (population.geneArray[i] * population.geneArray[i]) - 10 * math.cos(2*math.pi*population.geneArray[i])
            formula = 10 * config.n + sigma
            population.fitness = 1/formula
        return population





# crossover function    
def cross_over(population):
    index = 0
    children = []
    while(index < len(population)):
        # use whole algorithm for cross over
        # alpha value is 0.5
        newGene = []
        gene1 = population[index].geneArray
        index+=1
        gene2 = population[index].geneArray
        index+=1
        for i in range(0,len(gene1)):
            newGene.append( ( gene1[i]+gene2[i] ) / 2 )
        children.append( Chromosom(newGene) )
    newPopulation = children[:]
    newPopulation.extend(population)
    # return parents and children
    return newPopulation


def calculate_averageFitness(population):
    avg_value = 0
    for person in population:
        avg_value += person.fitness
    avg_value = avg_value/len(population)
    return avg_value

# self adaptation function
def selfAdaptation(population):
    parentsArray = population[:]
    # samples from a normal (Gaussian) distribution.
    sigmaValue = 1


    if config.functionSelection == 1:
        parentsArray = ChooseClass(config.populationCount).calculateFitnessFunctionOne(parentsArray)
    elif config.functionSelection == 2:
        parentsArray = ChooseClass(config.populationCount).calculateFitnessFunctionTwo(parentsArray)
    else :
        parentsArray = ChooseClass(config.populationCount).calculateFitnessFunctionThree(parentsArray)

    for index in range(0,config.generationCount):
        newPopulation = []
        # calculate fitness
        average_fitness = calculate_averageFitness(parentsArray)
        print('average_fitness:\t' + str(index) +'  '+ str(average_fitness))

        # calculate the population fitness and set it to population
        c = ChooseClass(config.populationCount)

        # create a copy as children Array

        # add the sigma value to genes
        for i in range(0,len(parentsArray)):
            person = Chromosom()
            person.geneArray = parentsArray[i].geneArray[:]
            person.fitness = -1

            for j in range(0,len(person.geneArray)):
                person.geneArray[j] += sigmaValue

                
            if config.functionSelection == 1:
                person = c.calculateFitnessFunctionOne(person)
            elif config.functionSelection == 2:
                person = c.calculateFitnessFunctionTwo(person)
            else :
                person = c.calculateFitnessFunctionThree(person)
                
            if(person.fitness > parentsArray[i].fitness):
                newPopulation.append( person )
            else:
                newPopulation.append(parentsArray[i])

        parentsArray = newPopulation[:]
        eta = 1 / math.sqrt( 2 * config.n)
        sigmaValue = sigmaValue * math.exp(eta * gauss(0,1))

    return parentsArray

def P_quintupleSuccess(population):
    # calculate the population fitness and set it to population
    if config.functionSelection == 1:
        print('using the first function')
        populationCopy = ChooseClass(config.populationCount).calculateFitnessFunctionOne(population[:])
    elif config.functionSelection == 2:
        print('using the second function')
        populationCopy = ChooseClass(config.populationCount).calculateFitnessFunctionTwo(population[:])
    else :
        print('using the third function')
        populationCopy = ChooseClass(config.populationCount).calculateFitnessFunctionThree(population[:])


    sigmaValue = 1


    for index in range(0,config.generationCount):
        
        c = ChooseClass(config.populationCount)

        success = 0
        newPopulation = []
        # mutate the genes with 90% probability
        # if(random.uniform(0,1) > 0.1):
        if 1==1:
            # add the sigma value to genes
            for i in range(0,len(populationCopy)):
                oldFitness = populationCopy[i].fitness

                # to make a shallow copy we needed to make a new object
                personCopy = Chromosom()
                personCopy.geneArray = populationCopy[i].geneArray[:]
                personCopy.fitness = -1

                gauss_value = gauss(0, sigmaValue)
                for j in range(0,len(personCopy.geneArray)):
                    personCopy.geneArray[j] += gauss_value
                
                personCopy = c.calculateFitnessFunctionOne(personCopy)


                # if our person got better fitness with population
                # we would append it to newpopulation
                # else we would append the old person
                if(oldFitness < personCopy.fitness):
                    success +=1
                    newPopulation.append( personCopy )
                elif (oldFitness >= personCopy.fitness) :
                    newPopulation.append(populationCopy[i])
        
        populationCopy = newPopulation[:]

        generationFitness = calculate_averageFitness(populationCopy)
        print('fitness generation number '+ str(index)+ '\t'+str(generationFitness))


        populationCopy = c.tournomant(newPopulation, True)                

        cValue = random.uniform(0.8,1)
        if( (success/len(populationCopy)) > 0.2 ):
            sigmaValue /=cValue
        elif (success/len(populationCopy)) < 0.2 :
            sigmaValue *=cValue

        # # crossover for genes with 10% probability
        if(random.uniform(0,1) < 0.1):
            populationCopy =  cross_over(populationCopy[:])

        

    return populationCopy


# initialize the population with random numbers
populationArray = []
def initPopulation():
    for i in range(0, config.populationCount):
        chromosom = Chromosom()
        populationArray.append(chromosom)
    return populationArray


# print('1. Run self adaptation algorithms')
# print('2. Run 1/5 success rule algorithms')
# print('3. Run both algorithms and show the results ')

# mode = input('please enter the to use which algorithms (Numbers only)')
mode = '1'

#initialize population
populationArray = initPopulation()
if(mode == '2'):
    print('mode 2 selected!')
    print('\n\n1/5 success results Adaptation results')    
    P_quintupleSuccess(populationArray)
elif mode == '1':
    print('mode 1 selected!')
    print('\n\nSelft Adaptation results Adaptation results')    
    selfAdaptation(populationArray)
else:
    print('mode third selected!')
    print('\n\n1/5 success results Adaptation results')    
    P_quintupleSuccess(populationArray)

    print('\n\nSelft Adaptation results Adaptation results')    
    selfAdaptation(populationArray)

